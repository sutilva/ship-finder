## Installing NPM packages

npm i .  -- Run this code on git-bash (Go to Project Directory) and the "." dot in the end is important to install all the node modules

## Running the code

node app.js -- Run this code on git-bash (In project Directory)

## Localhost path

Open the browser and go to http://localhost:3000/

## Search Ship

Enter valid Ship's MMSI# and click submit button
