const express = require("express");
const bodyParser = require("body-parser");
const request = require("request");
const https = require("https");
const axios = require("axios");
const thenWriteJson = require('then-write-json');
const fse = require("fs-extra");
const app = express();

app.use(express.static("public"));
app.use(bodyParser.urlencoded({extended: true}));


// Fetching index.html file from the project directory
app.get("/", function(req, res){
  res.sendFile(__dirname + "/index.html");
});

app.post("/", function(req, res){

  var mmsiNumber = parseInt(req.body.mmsi_number); //User input MMSI Number

  var filePath = __dirname + "/Database/" + mmsiNumber + ".json"; //File Path of Database. Read and write data from this path.

  // Reading file from database folder
  fse.readFile(filePath, "utf-8", function(err, jsonString){
    if(err){

      // HTTPS request to fetch Ship Data
      url = "https://services.marinetraffic.com/api/vesselmasterdata/8149ccb1f11be5bad7baaf9f19809a88832efc9f?v=4&mmsi="+mmsiNumber+"&interval=minutes&protocol=json&page=1";

      https.get(url, function(response){
        response.on("data", function(data){
          const shipData = JSON.parse(data);

          // Storing requested data into Database folder in the form JSON File
          const tempData = shipData;


          const jsonSchema = {
            "mmsi": tempData[0][0],
            "imo": tempData[0][1],
            "name": tempData[0][2],
            "build": tempData[0][4],
            "vessel-type": tempData[0][20]
          }
          const dbData = JSON.stringify(jsonSchema, null, 2);


          const storeData = fse.writeFile(filePath, dbData, function(err) {
            console.log(err);
          });

          // Display Data
          displayData(shipData);
          res.send();

        });
      });
    }
    else {

      // Reading Data from existing file from Database Folder
      const shipData = JSON.parse(jsonString);

      displayData(shipData);
      res.write("<h1>Data fetched from the Database</h1>");
      res.send();
      console.log(shipData);
      console.log("From file");
    }
  })

  // Function to Display Data from API
  function displayData(data) {
    const mmsiNum = data[0][0];
    const imo = data[0][1];
    const name = data[0][2];
    const buildYear = data[0][4];
    const vsesselType = data[0][20];
    res.write("<h1>MMSI: " + data[0][0] + "</h1>");
    res.write("<h1>IMO: " + data[0][1] + "</h1>");
    res.write("<h1>Name: " + data[0][2] + "</h1>");
    res.write("<h1>Build_Year: " + data[0][4] + "</h1>");
    res.write("<h1>Vessel_Type: " + data[0][20] + "</h1>");
    //res.send();
  }

});

app.listen(3000, function(){
  console.log("Server Running on port 3000: ");
});








// 316042572
// 6260354
// 9781853
